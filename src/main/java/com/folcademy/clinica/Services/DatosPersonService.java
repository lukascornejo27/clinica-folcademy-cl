package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Models.Dtos.DatosPersonDto;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Entities.Paciente;
import com.folcademy.clinica.Models.Mappers.DatosPersonMapper;
import com.folcademy.clinica.Models.Repositories.DatosPersonRepository;
import com.folcademy.clinica.Models.Repositories.MedicoRepository;
import com.folcademy.clinica.Models.Repositories.PacienteRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class DatosPersonService {
    private final DatosPersonRepository datosPersonRepository;
    private final DatosPersonMapper datosPersonMapper;
    private final PacienteRepository pacienteRepository;
    private final MedicoRepository medicoRepository;

    public DatosPersonService(DatosPersonRepository datosPersonRepository, DatosPersonMapper datosPersonMapper, PacienteRepository pacienteRepository, MedicoRepository medicoRepository) {
        this.datosPersonRepository = datosPersonRepository;
        this.datosPersonMapper = datosPersonMapper;
        this.pacienteRepository = pacienteRepository;
        this.medicoRepository = medicoRepository;
    }

    public List<DatosPersonDto> findAll(Integer pageNo, Integer pageSize, String sortBy) {

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<DatosPersonDto> pagedResult = datosPersonRepository.findAll(paging).map(datosPersonMapper::entityToDto);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }

    public DatosPersonDto findOne(Integer id){
        if(!datosPersonRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        return datosPersonRepository.findById(id).map(datosPersonMapper::entityToDto).orElse(null);
    }

    public DatosPersonDto newDatosPerson(DatosPersonDto entity){
        if(entity.getDni().equals("") ){
            throw new BadRequestException("El dni es obligatorio");
        }
        entity.setId(null);
        var datos = datosPersonMapper.entityToDto(datosPersonRepository.save(datosPersonMapper.dtoToEntity(entity)));

        return datos;

    }

    public void delete(Integer id) {
        if(!datosPersonRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        datosPersonRepository.deleteById(id);
        return;
    }

    public DatosPersonDto edit(Integer id, DatosPersonDto datosPersonDto){
        if(!datosPersonRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        else {
            datosPersonDto.setId(id);
            return datosPersonMapper.entityToDto(
                    datosPersonRepository.save(
                            datosPersonMapper.dtoToEntity(datosPersonDto)
                    )
            );
        }
    }
}
