package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import java.util.List;

public interface IMedicoService {
    List<MedicoDto> findAll(Integer pageNo,Integer pageSize,String sortBy);
}