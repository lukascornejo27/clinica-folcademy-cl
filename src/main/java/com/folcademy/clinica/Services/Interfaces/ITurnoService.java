package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Models.Dtos.TurnoVistaDto;
import java.util.List;

public interface ITurnoService {
    List<TurnoVistaDto> findAll(Integer pageNo,Integer pageSize,String sortBy);
}
