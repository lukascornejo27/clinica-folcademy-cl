package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Dtos.PacienteDtoEdit;
import com.folcademy.clinica.Models.Mappers.DatosPersonMapper;
import com.folcademy.clinica.Models.Mappers.PacienteMapper;
import com.folcademy.clinica.Models.Repositories.DatosPersonRepository;
import com.folcademy.clinica.Models.Repositories.PacienteRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacienteService implements IPacienteService {

    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final DatosPersonRepository datosPersonRepository;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, DatosPersonRepository datosPersonRepository, DatosPersonMapper datosPersonMapper, DatosPersonRepository datosPersonRepository1){
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.datosPersonRepository = datosPersonRepository1;
    }


    @Override
    public List<PacienteDto> findAll(Integer pageNo,Integer pageSize,String sortBy) {

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<PacienteDto> pagedResult = pacienteRepository.findAll(paging).map(pacienteMapper::entityToDto);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }
    public PacienteDto findOne(Integer id){
        if(!pacienteRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
    }

    public PacienteDto newPaciente(PacienteDto entity){

        if(entity.getId_datos_person() == null ){
            throw new BadRequestException("Debe ingresar un valor id de datos_person");
        }
        if(!datosPersonRepository.existsById(entity.getId_datos_person())){
            throw new NotFoundException("No se encontro el ID de datos_person");
        }
        entity.setId(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }

    public void delete(Integer id){
        if(!pacienteRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID de medico");
        }
        var id_datos = pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null).getId_datos_person();

        pacienteRepository.deleteById(id);
        if(id_datos!= null){
            datosPersonRepository.deleteById(id_datos);
        }
    }

    public PacienteDtoEdit edit(Integer id, PacienteDtoEdit pacienteDto){

        if(!pacienteRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        else if(pacienteDto.getId_datos_person() == null ){
            throw new BadRequestException("Debe ingresar un valor id de datos_person");
        }
        else {
            pacienteDto.setId(id);
            return pacienteMapper.entityToDtoEdit(
                    pacienteRepository.save(
                            pacienteMapper.dtoToEntityEdit(pacienteDto)
                    )
            );
        }
    }

}
