package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Models.Dtos.TurnoVistaDto;
import com.folcademy.clinica.Models.Mappers.TurnoMapper;
import com.folcademy.clinica.Models.Repositories.MedicoRepository;
import com.folcademy.clinica.Models.Repositories.PacienteRepository;
import com.folcademy.clinica.Models.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TurnoService implements ITurnoService {

    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;
    private final MedicoRepository medicoRepository;
    private final PacienteRepository pacienteRepository;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper, MedicoRepository medicoRepository, PacienteRepository pacienteRepository) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
        this.medicoRepository = medicoRepository;
        this.pacienteRepository = pacienteRepository;
    }
    @Override
    public List<TurnoVistaDto> findAll(Integer pageNo,Integer pageSize,String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<TurnoVistaDto> pagedResult = turnoRepository.findAll(paging).map(turnoMapper::entityToDtoTurnoVista);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }
    public TurnoVistaDto findOne(Integer id){
        if(!turnoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        return turnoRepository.findById(id).map(turnoMapper::entityToDtoTurnoVista).orElse(null);
    }
    public TurnoDto newTurno(TurnoDto entity){
        if(!pacienteRepository.existsById(entity.getIdpaciente()) || !medicoRepository.existsById(entity.getIdmedico())){
            throw new BadRequestException("El id del medico o paciente es invalido");
        }
        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(entity)));
    }

    public void delete(Integer id){
        if(!turnoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        turnoRepository.deleteById(id);
    }

    public TurnoDto edit(Integer id, TurnoDto turnoDto){
        if(!turnoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        else {
            turnoDto.setId(id);
            return turnoMapper.entityToDto(
                    turnoRepository.save(
                            turnoMapper.dtoToEntity(turnoDto)
                    )
            );
        }
    }

}
