package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.MedicoDtoEdit;
import com.folcademy.clinica.Models.Mappers.MedicoMapper;
import com.folcademy.clinica.Models.Repositories.DatosPersonRepository;
import com.folcademy.clinica.Models.Repositories.MedicoRepository;
import com.folcademy.clinica.Services.Interfaces.IMedicoService;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class MedicoService implements IMedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final DatosPersonRepository datosPersonRepository;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, DatosPersonRepository datosPersonRepository){
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.datosPersonRepository = datosPersonRepository;
    }


    public List<MedicoDto> findAll(Integer pageNo,Integer pageSize,String sortBy) {

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));

        Page<MedicoDto> pagedResult = medicoRepository.findAll(paging).map(medicoMapper::entityToDto);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<>();
        }
    }

    public MedicoDto findOne(Integer id){
        if(!medicoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID");
        }
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }

    public MedicoDto newMedico(MedicoDto entity){
        if(entity.getConsulta() < 0 ){
            throw new BadRequestException("El precio de la consulta debe ser mayor a 0");
        }
        if(entity.getId_datos_person() == null ){
            throw new BadRequestException("Debe ingresar un valor id de datos_person");
        }
        if(!datosPersonRepository.existsById(entity.getId_datos_person())){
            throw new NotFoundException("No se encontro el ID de datos_person");
        }
        entity.setId(null);
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }

    public void delete(Integer id) {
        if(!medicoRepository.existsById(id)){
            throw new NotFoundException("No se encontro el ID de medico");
        }
        var id_datos = medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null).getId_datos_person();


        medicoRepository.deleteById(id);
        if(id_datos!= null){
            datosPersonRepository.deleteById(id_datos);
        }

    }

    public MedicoDtoEdit edit(Integer id, MedicoDtoEdit medicoDto){
        if(!medicoRepository.existsById(id)){
           throw new NotFoundException("No se encontro el ID");
        }
        else if(medicoDto.getId_datos_person() == null ){
            throw new BadRequestException("Debe ingresar un valor id de datos_person");
        }
        else {
            medicoDto.setId(id);
            return medicoMapper.entityToDtoEdit(
                    medicoRepository.save(
                            medicoMapper.dtoToEntityEdit(medicoDto)
                    )
            );
        }
    }
}
