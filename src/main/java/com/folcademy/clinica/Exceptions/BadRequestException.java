package com.folcademy.clinica.Exceptions;

public class BadRequestException extends RuntimeException{

    public BadRequestException(String msj){
        super(msj);
    }

}
