package com.folcademy.clinica.Exceptions;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String msj){
        super(msj);
    }
}
