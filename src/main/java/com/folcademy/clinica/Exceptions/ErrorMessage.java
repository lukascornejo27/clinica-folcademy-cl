package com.folcademy.clinica.Exceptions;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {
    private String message;
    private String detail;
    private String path;
    private  Exception code;

}
