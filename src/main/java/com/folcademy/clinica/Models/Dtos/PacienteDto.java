package com.folcademy.clinica.Models.Dtos;

import com.folcademy.clinica.Models.Entities.DatosPerson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDto {
    Integer id;
    String direccion;
    Integer id_datos_person;
    DatosPerson datosPerson;
}

