package com.folcademy.clinica.Models.Dtos;

import com.folcademy.clinica.Models.Entities.DatosPerson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDto {
    Integer id;
    String Profesion;
    Integer Consulta;
    Integer id_datos_person;
    DatosPerson datosPerson;

}
