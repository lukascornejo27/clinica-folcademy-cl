package com.folcademy.clinica.Models.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoDtoEdit {
    Integer id;
    String Profesion;
    Integer Consulta;
    Integer id_datos_person;
}
