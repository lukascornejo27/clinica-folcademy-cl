package com.folcademy.clinica.Models.Dtos;

import com.folcademy.clinica.Models.Entities.Medico;
import com.folcademy.clinica.Models.Entities.Paciente;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoVistaDto {
    Integer id;
    Paciente paciente;
    Medico medico;
    String fecha;
    String hora;
    int atendido;

}
