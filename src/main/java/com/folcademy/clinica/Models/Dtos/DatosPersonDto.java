package com.folcademy.clinica.Models.Dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatosPersonDto {
    Integer Id;
    String Nombre;
    String Apellido;
    Integer Dni;
    Integer Telefono;
}
