package com.folcademy.clinica.Models.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteDtoEdit {
    Integer id;
    String direccion;
    Integer id_datos_person;
}
