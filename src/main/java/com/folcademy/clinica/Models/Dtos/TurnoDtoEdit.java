package com.folcademy.clinica.Models.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDtoEdit {
    Integer id;
    String fecha;
    String hora;
    int atendido;

}
