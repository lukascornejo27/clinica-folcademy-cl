package com.folcademy.clinica.Models.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    Integer id;
    Integer idpaciente;
    Integer idmedico;
    String fecha;
    String hora;
    int atendido;

}
