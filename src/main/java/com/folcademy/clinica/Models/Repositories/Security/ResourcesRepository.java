package com.folcademy.clinica.Models.Repositories.Security;

import com.folcademy.clinica.Models.Entities.Security.Resource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ResourcesRepository extends JpaRepository<Resource,Integer> {
}
