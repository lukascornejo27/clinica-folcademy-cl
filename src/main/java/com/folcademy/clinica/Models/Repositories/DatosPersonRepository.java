package com.folcademy.clinica.Models.Repositories;

import com.folcademy.clinica.Models.Entities.DatosPerson;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DatosPersonRepository extends PagingAndSortingRepository<DatosPerson,Integer> {
}
