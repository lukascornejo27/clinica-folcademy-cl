package com.folcademy.clinica.Models.Repositories;

import com.folcademy.clinica.Models.Entities.Turno;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurnoRepository extends PagingAndSortingRepository<Turno,Integer> {

}
