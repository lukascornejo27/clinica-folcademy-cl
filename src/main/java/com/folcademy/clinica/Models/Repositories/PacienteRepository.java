package com.folcademy.clinica.Models.Repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import com.folcademy.clinica.Models.Entities.Paciente;

@Repository
public interface PacienteRepository extends PagingAndSortingRepository<Paciente, Integer>{

}
