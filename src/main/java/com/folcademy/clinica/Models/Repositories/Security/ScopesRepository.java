package com.folcademy.clinica.Models.Repositories.Security;

import com.folcademy.clinica.Models.Entities.Security.Scope;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScopesRepository extends JpaRepository<Scope,Integer> {
}
