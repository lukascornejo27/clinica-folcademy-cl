package com.folcademy.clinica.Models.Repositories;

import com.folcademy.clinica.Models.Entities.Medico;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicoRepository extends PagingAndSortingRepository<Medico, Integer> {

}
