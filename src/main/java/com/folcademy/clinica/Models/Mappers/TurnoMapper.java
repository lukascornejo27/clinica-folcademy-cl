package com.folcademy.clinica.Models.Mappers;

import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Models.Dtos.TurnoDtoEdit;
import com.folcademy.clinica.Models.Dtos.TurnoVistaDto;
import com.folcademy.clinica.Models.Entities.Turno;
import org.springframework.stereotype.Component;
import java.sql.Date;
import java.sql.Time;
import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getIdturno(),
                                ent.getIdpaciente(),
                                ent.getIdmedico(),
                                String.valueOf(ent.getFecha()),
                                String.valueOf(ent.getHora()),
                                ent.getAtendido()
                        )
                )
                .orElse(new TurnoDto());
    }
    public Turno dtoToEntity(TurnoDto dto){
        Turno entity = new Turno();
        entity.setIdturno(dto.getId());
        entity.setIdmedico(dto.getIdmedico());
        entity.setIdpaciente (dto.getIdpaciente());
        entity.setFecha(Date.valueOf(dto.getFecha()));
        entity.setHora(Time.valueOf(dto.getHora()));
        entity.setAtendido(dto.getAtendido());

        return entity;
    }

    public TurnoDtoEdit entityToDtoTurnoEdit(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDtoEdit(
                                ent.getIdturno(),
                                String.valueOf(ent.getFecha()),
                                String.valueOf(ent.getHora()),
                                ent.getAtendido()
                        )
                )
                .orElse(new TurnoDtoEdit())
        ;
    }

    public Turno dtoToEntityTurnoEdit(TurnoDtoEdit dto){
        Turno entity = new Turno();
        //entity.setIdturno(dto.getId());
        entity.setFecha(Date.valueOf(dto.getFecha()));
        entity.setHora(Time.valueOf(dto.getHora()));
        entity.setAtendido(dto.getAtendido());

        return entity;
    }

    public TurnoVistaDto entityToDtoTurnoVista(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoVistaDto(
                                ent.getIdturno(),
                                ent.getPaciente(),
                                ent.getMedico(),
                                String.valueOf(ent.getFecha()),
                                String.valueOf(ent.getHora()),
                                ent.getAtendido()
                                )
                )
                .orElse(new TurnoVistaDto())
                ;
    }

}
