package com.folcademy.clinica.Models.Mappers;

import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Dtos.PacienteDtoEdit;
import com.folcademy.clinica.Models.Entities.Paciente;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getIdpaciente(),
                                ent.getDireccion(),
                                ent.getId_datos_person(),
                                ent.getDatosPerson()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();

        entity.setIdpaciente(dto.getId());
        entity.setDireccion(dto.getDireccion());
        entity.setId_datos_person(dto.getId_datos_person());
        entity.setDatosPerson(dto.getDatosPerson());

        return entity;
    }

    public Paciente dtoToEntityEdit(PacienteDtoEdit dto){
        Paciente entity = new Paciente();

        entity.setIdpaciente(dto.getId());
        entity.setDireccion(dto.getDireccion());
        entity.setId_datos_person(dto.getId_datos_person());

        return entity;
    }

    public PacienteDtoEdit entityToDtoEdit(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDtoEdit(

                                ent.getIdpaciente(),
                                ent.getDireccion(),
                                ent.getId_datos_person()
                        )
                )
                .orElse(new PacienteDtoEdit());
    }
}
