package com.folcademy.clinica.Models.Mappers;

import com.folcademy.clinica.Models.Dtos.DatosPersonDto;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Entities.DatosPerson;
import com.folcademy.clinica.Models.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class DatosPersonMapper {
    public DatosPersonDto entityToDto(DatosPerson entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new DatosPersonDto(
                                ent.getId_datos_person(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getDni(),
                                ent.getTelefono()
                        )
                )
                .orElse(new DatosPersonDto());
    }

    public DatosPerson dtoToEntity(DatosPersonDto dto){
        DatosPerson entity = new DatosPerson();

        entity.setId_datos_person(dto.getId());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getTelefono());

        return entity;
    }
}
