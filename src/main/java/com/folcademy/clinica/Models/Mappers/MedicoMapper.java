package com.folcademy.clinica.Models.Mappers;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.MedicoDtoEdit;
import com.folcademy.clinica.Models.Entities.Medico;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class MedicoMapper {

    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getIdmedico(),
                                ent.getProfesion(),
                                ent.getConsuta(),
                                ent.getId_datos_person(),
                                ent.getDatosPerson()
                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();
        entity.setIdmedico(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsuta(dto.getConsulta());
        entity.setId_datos_person(dto.getId_datos_person());
        entity.setDatosPerson(dto.getDatosPerson());

        return entity;
    }

    public MedicoDtoEdit entityToDtoEdit(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDtoEdit(
                                ent.getIdmedico(),
                                ent.getProfesion(),
                                ent.getConsuta(),
                                ent.getId_datos_person()
                        )
                )
                .orElse(new MedicoDtoEdit());
    }
    public Medico dtoToEntityEdit(MedicoDtoEdit dto){
        Medico entity = new Medico();
        entity.setIdmedico(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsuta(dto.getConsulta());
        entity.setId_datos_person(dto.getId_datos_person());

        return entity;
    }
}
