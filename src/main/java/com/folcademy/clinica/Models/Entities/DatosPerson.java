package com.folcademy.clinica.Models.Entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name ="datos_person")
public class DatosPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_datos_person", columnDefinition = "INT(10) UNSIGNED")
    public Integer id_datos_person;
    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    public String nombre ="";
    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    public String apellido="";
    @Column(name = "telefono", columnDefinition = "INT")
    public Integer telefono;
    @Column(name = "dni", columnDefinition = "INT")
    public Integer dni;

}
