package com.folcademy.clinica.Models.Entities;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import javax.persistence.*;

@Entity
@Table(name ="paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;
    @Column(name = "direccion", columnDefinition = "VARCHAR")
    public String direccion="";
    @NotNull
    @Column(name = "id_datos_person", columnDefinition = "INT(10) UNSIGNED")
    public Integer id_datos_person;

    @OneToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_datos_person",insertable = false,updatable = false)
    private DatosPerson datosPerson;

}
