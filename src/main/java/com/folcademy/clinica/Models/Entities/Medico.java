package com.folcademy.clinica.Models.Entities;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Table(name ="medico")
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer idmedico;
    @Column(name = "Profesion", columnDefinition = "VARCHAR")
    public String profesion="";
    @Column(name = "Consulta", columnDefinition = "INT")
    public Integer consuta;
    @NotNull
    @Column(name = "id_datos_person", columnDefinition = "INT(10) UNSIGNED")
    public Integer id_datos_person;

    @OneToOne()
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "id_datos_person",referencedColumnName = "id_datos_person",insertable = false,updatable = false)
    private DatosPerson datosPerson;
}
