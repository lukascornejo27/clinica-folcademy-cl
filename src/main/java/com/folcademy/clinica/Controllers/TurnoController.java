package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Dtos.TurnoDto;
import com.folcademy.clinica.Models.Dtos.TurnoDtoEdit;
import com.folcademy.clinica.Models.Dtos.TurnoVistaDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/turnos")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    @PreAuthorize("hasAuthority('turno_find')")
    @GetMapping(value ="")
    public ResponseEntity<List<TurnoVistaDto>> findAll(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize,
            @RequestParam(defaultValue = "idturno") String sortBy
    ){
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAll(
                                pageNo, pageSize, sortBy
                        )
                )
                ;
    }
    @PreAuthorize("hasAuthority('turno_find')")
    @GetMapping(value ="/{id}")
    public ResponseEntity<TurnoVistaDto> findOne(@PathVariable(name="id") Integer id){
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findOne(id)
                )
                ;

    }
    @PreAuthorize("hasAuthority('turno_new')")
    @PostMapping(value = "/new")
    public ResponseEntity<TurnoDto> save(@RequestBody @Validated TurnoDto entity){
        return ResponseEntity
                .ok()
                .body(
                        turnoService.newTurno(entity)
                )
                ;
    }
    @PreAuthorize("hasAuthority('turno_delete')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name="id") Integer id){
        if(turnoService.findOne(id) != null){
            turnoService.delete(id);
            return ResponseEntity.ok().build();
        }
        else{
            return  ResponseEntity.notFound().build();
        }

    }
    @PreAuthorize("hasAuthority('turno_put')")
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<?> editOne(@PathVariable(name="id") Integer id,@RequestBody TurnoDto dto){
        if(turnoService.findOne(id) != null){
            return ResponseEntity.ok().body(turnoService.edit(id,dto));
        }
        else{
            return  ResponseEntity.notFound().build();
        }

    }
}
