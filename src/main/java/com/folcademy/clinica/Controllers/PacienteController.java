package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Models.Dtos.PacienteDtoEdit;
import com.folcademy.clinica.Models.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    @PreAuthorize("hasAuthority('paciente_find')")
    @GetMapping(value ="")
    public ResponseEntity<List<PacienteDto>> findAll(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize,
            @RequestParam(defaultValue = "idpaciente") String sortBy
    ){
            return ResponseEntity
                    .ok()
                    .body(
                            pacienteService.findAll(
                                    pageNo, pageSize, sortBy
                            )
                    )
            ;
    }
    @PreAuthorize("hasAuthority('paciente_find')")
    @GetMapping(value ="/{id}")
    public ResponseEntity<PacienteDto> findOne(@PathVariable(name="id") Integer id){
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findOne(id)
                )
                ;

    }
    @PreAuthorize("hasAuthority('paciente_new')")
    @PostMapping(value = "/new")
    public ResponseEntity<PacienteDto> savePaciente(@RequestBody @Validated PacienteDto entity){
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.newPaciente(entity)
                )
                ;
    }
    @PreAuthorize("hasAuthority('paciente_delete')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name="id") Integer id){

            pacienteService.delete(id);
            return ResponseEntity.ok().build();

    }
    @PreAuthorize("hasAuthority('paciente_put')")
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<?> editOne(@PathVariable(name="id") Integer id,@RequestBody PacienteDtoEdit dto){
        if(pacienteService.findOne(id) != null){
            return ResponseEntity.ok().body(pacienteService.edit(id,dto));
        }
        else{
            return  ResponseEntity.notFound().build();
        }

    }



}
