package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.MedicoDtoEdit;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/medicos")
public class MedicoController {

    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }
    @PreAuthorize("hasAuthority('medico_find')")
    @GetMapping(value ="")
    public ResponseEntity<List<MedicoDto>> findAll(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize,
            @RequestParam(defaultValue = "idmedico") String sortBy
    ){
        return ResponseEntity
                .ok()
                .body(
                        medicoService.findAll(
                                pageNo, pageSize, sortBy
                        )
                )
                ;

    }
    @PreAuthorize("hasAuthority('medico_find')")
    @GetMapping(value ="/{id}")
    public ResponseEntity<MedicoDto> findOne(
            @PathVariable(name="id") Integer id
    ){
        return ResponseEntity
                .ok()
                .body(
                        medicoService.findOne(id)
                )
        ;

    }
    @PreAuthorize("hasAuthority('medico_new')")
    @PostMapping(value = "/new")
    public ResponseEntity<MedicoDto> saveMedico(@RequestBody @Validated MedicoDto entity){
        return ResponseEntity
                .ok()
                .body(
                        medicoService.newMedico(entity)
                )
        ;
    }
    @PreAuthorize("hasAuthority('medico_delete')")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<?> deleteOne(
            @PathVariable(name="id") Integer id
    ){
            medicoService.delete(id);
            return ResponseEntity.ok().build();

    }
    @PreAuthorize("hasAuthority('medico_put')")
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<?> editOne(@PathVariable(name="id") Integer id,@RequestBody MedicoDtoEdit dto){
       
        return ResponseEntity.ok().body(medicoService.edit(id,dto));

    }


}
