package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Models.Dtos.DatosPersonDto;
import com.folcademy.clinica.Models.Dtos.MedicoDto;
import com.folcademy.clinica.Models.Dtos.PacienteDto;
import com.folcademy.clinica.Services.DatosPersonService;
import com.folcademy.clinica.Services.MedicoService;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/datos")
public class DatosPersonController {

    private final DatosPersonService datosPersonService;


    public DatosPersonController(DatosPersonService datosPersonService) {
        this.datosPersonService = datosPersonService;
    }

    @PreAuthorize("hasAuthority('datos_person')")
    @PostMapping("/add")
    public ResponseEntity<DatosPersonDto> addDatos(
            @Validated
            @RequestBody DatosPersonDto entity
    ){
        return ResponseEntity
                .ok()
                .body(
                        datosPersonService.newDatosPerson(entity)
                )
                ;
    }

    @PreAuthorize("hasAuthority('datos_person')")
    @PutMapping(value = "/edit/{id}")
    public ResponseEntity<?> editOne(@PathVariable(name="id") Integer id,@RequestBody DatosPersonDto dto){

        return ResponseEntity.ok().body(datosPersonService.edit(id,dto));

    }

    @PreAuthorize("hasAuthority('datos_person')")
    @GetMapping(value ="")
    public ResponseEntity<List<DatosPersonDto>> findAll(
            @RequestParam(defaultValue = "0") Integer pageNo,
            @RequestParam(defaultValue = "5") Integer pageSize,
            @RequestParam(defaultValue = "apellido") String sortBy
    ){
        return ResponseEntity
                .ok()
                .body(
                        datosPersonService.findAll(
                                pageNo, pageSize, sortBy
                        )
                )
                ;

    }

    @PreAuthorize("hasAuthority('datos_person')")
    @GetMapping(value ="/{id}")
    public ResponseEntity<DatosPersonDto> findOne(
            @PathVariable(name="id") Integer id
    ){
        return ResponseEntity
                .ok()
                .body(
                        datosPersonService.findOne(id)
                )
                ;

    }

}
